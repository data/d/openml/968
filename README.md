# OpenML dataset: analcatdata_birthday

https://www.openml.org/d/968

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Binarized version of the original data set (see version 1). The multi-class target feature is converted to a two-class nominal target feature by re-labeling the majority class as positive ('P') and all others as negative ('N'). Originally converted by Quan Sun.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/968) of an [OpenML dataset](https://www.openml.org/d/968). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/968/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/968/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/968/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

